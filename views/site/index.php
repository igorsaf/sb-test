<?php
/**
 * @var \app\models\Maps $map
 */
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
?>
<div class="row">
    <div class="col-xs-6 col-xs-offset-3">
        <div class="mapContainer">
            <?php Pjax::begin()?>
                <table class="map">
                    <tbody>
                    <tr><?= implode(
                            '</tr><tr>',
                            array_map(function ($row) {
                                return implode('', array_map(function ($cell) {
                                    return $cell === 0 ? '<td>&nbsp;</td>' : '<td class="active">&nbsp;</td>';
                                }, $row));
                            }, $map->map))?></tr>
                    </tbody>
                </table>
                <?= Html::a('Generate new map', Url::to('/'), ['class' => 'btn btn-success'])?>
            <?php if (empty($map->id)) : ?>
                <?= Html::beginForm('', 'post', ['data-pjax' => '']); ?>
                    <?= Html::input('hidden', 'data', $map->data)?>
                    <?= Html::submitButton('Save map', ['class' => 'btn btn-primary btn-lg'])?>
                <?php Html::endForm()?>
            <?php else : $url = Url::to(['site/index', 'mapId' => $map->id], true); ?>
                <h3>Link for this map - <?= Html::a($url, $url, ['data-pjax' => '0'])?></h3>
            <?php endif;?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>
