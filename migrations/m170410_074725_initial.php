<?php

use yii\db\Migration;

class m170410_074725_initial extends Migration
{
    public function up()
    {
        $this->createTable('maps', [
            'id' => $this->primaryKey(),
            'data' => $this->string(79)->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('maps');
    }

}
