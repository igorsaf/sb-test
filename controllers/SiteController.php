<?php

namespace app\controllers;

use Yii;
use yii\helpers\Url;
use yii\web\Controller;
use app\models\Maps;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex($mapId = 0)
    {
        if (empty($mapId)) {
            $map = new Maps();
            if (Yii::$app->request->isPjax && !empty($mapData = Yii::$app->request->post('data'))) {
                $map->data = $mapData;
                $map->save();
                $map->convertCoordinatesToMap();
                Yii::$app->response->getHeaders()->set('X-PJAX-Url', Url::to(['site/index', 'mapId' => $map->id]));
            } else {
                $map->generateRandomMap();
            }
        } else {
            $map = Maps::findOne($mapId);
            if ($map === NULL) {
                return $this->render('noMapWithId', ['id' => $mapId]);
            } else {
                $map->convertCoordinatesToMap();
            }
        }

        return $this->render('index', [
            'map' => $map
        ]);
    }
}
