<?php

namespace app\models;

use yii\db\ActiveRecord;

class Maps extends ActiveRecord
{

    public $map = [];

    private function generateEmptyMap()
    {
        return array_map(function () {
            return array_map(function () {
                return 0;
            }, range(0, 9));
        }, range(0, 9));
    }

    private function placeShip($shipLength, $startX, $startY, $isVertical)
    {

        $endX = $isVertical ? $startX : $startX + $shipLength - 1;
        $endY = $isVertical ? $startY + $shipLength - 1 : $startY;

        for ($i = $startX-1; $i <= $endX+1; $i++) {
            for ($j = $startY-1; $j <= $endY+1; $j++) {

                if (!isset($this->map[$i][$j]) || $this->map[$i][$j] !== 0) {

                    return false;
                }
            }
        }

        for ($i = $startX; $i <= $endX; $i++) {
            for ($j = $startY; $j <= $endY; $j++) {
                $this->map[$i][$j] = 1;
            }
        }

        return true;
    }

    public function generateRandomMap()
    {
        $this->map = $this->generateEmptyMap();

        $mapGeneratedSuccessful = true;

        $ships = [
            4,
            3, 3,
            2, 2, 2,
            1, 1, 1, 1,
        ];

        foreach ($ships as $shipLength) {
            $shipIsPlaced = false;

            $placesToCheck = [];
            for ($i = 0; $i <= 9; $i++) {
                for ($j = 0; $j <= 9; $j++) {
                    if ($this->map[$i][$j] === 0)
                    foreach (['h', 'v'] as $direction) {
                        $placesToCheck[] = [$i, $j, $direction];
                    }
                }
            }

            while (!$shipIsPlaced) {
                $randomPlaceKey = array_rand($placesToCheck);
                if ($randomPlaceKey === NULL) {
                    break;
                }
                $randomPlace = $placesToCheck[$randomPlaceKey];
                unset($placesToCheck[$randomPlaceKey]);

                $shipIsPlaced = $this->placeShip($shipLength, $randomPlace[0], $randomPlace[1], $randomPlace[2] === 'v');
            }

            if (!$shipIsPlaced) {
                $mapGeneratedSuccessful = false;
                break;
            }
        }

        if (!$mapGeneratedSuccessful) {
            $this->generateRandomMap();
        }

        $this->convertMapToCoordinates();

    }

    public function convertMapToCoordinates()
    {
        if (empty($this->map)) $this->data = '';

        $coordinates = [];
        foreach ($this->map as $i => $row) {
            foreach ($row as $j => $cell) {
                if ($cell !== 0) {
                    $coordinates[] = $j.','.$i;
                }
            }
        }

        $this->data = implode(';', $coordinates);

    }

    public function convertCoordinatesToMap()
    {
        $map = $this->generateEmptyMap();

        if (!empty($this->data)) {
            $coordinatesArray = array_map(function ($ship) {
                return explode(',', $ship);
            }, explode(';',$this->data));

            foreach ($coordinatesArray as $ship) {
                $map[$ship[0]][$ship[1]] = 1;
            }
        }

        $this->map = $map;
    }



}