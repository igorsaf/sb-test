SeaBattle
============================

### Installation

1. Clone repo
2. Install composer dependencies - in root directory execute `composer install`
3. Setup database in `config/db.php`
4. Create tables in database - in root directory execute `php yii migrate\up`
